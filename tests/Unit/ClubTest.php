<?php

namespace Tests\Unit;

use App\Club\Behaviors\Dances\Electrodance;
use App\Club\Behaviors\Dances\Hiphop;
use App\Club\Behaviors\Dances\House;
use App\Club\Club;
use App\Club\Genres\Electrohouse;
use App\Club\Genres\Rnb;
use Tests\TestCase;

class ClubTest extends TestCase {
    /**
     * @var Club
     */
    protected $club;

    protected function setUp()
    {
        parent::setUp();
        $this->club = Club::getInstance();
    }

    /**
     * Test init with empty data.
     *
     * @return void
     */
    public function testInitWithEmptyData()
    {
        $data = [
            'playlist'     => [],
            'people'       => [],
            'current_song' => 0,
        ];
        $this->club->init($data['people'], $data['playlist'], $data['current_song']);
    }

    public function testDescribeWithEmptyData()
    {
        $described = $this->club->describe();
        $this->assertArrayHasKey('genres', $described);
        $this->assertNotEmpty($described['genres']);
        $this->assertThat($described['genres'][0], $this->arrayHasKey('id'));
        $this->assertThat($described['genres'][0], $this->arrayHasKey('name'));

        $this->assertArrayHasKey('genders', $described);
        $this->assertNotEmpty($described['genders']);

        $this->assertArrayHasKey('levels', $described);
        $this->assertNotEmpty($described['levels']);

        $this->assertArrayHasKey('dances', $described);
        $this->assertNotEmpty($described['dances']);

        $this->assertArrayHasKey('playlist', $described);
        $this->assertArrayHasKey('currentSong', $described);

        $this->assertEquals(0, $described['currentSong']);

        $this->assertArrayHasKey('people', $described);
    }

    public function testWithFilledData()
    {
        $data = [
            'playlist'     => [
                [
                    'genre' => Electrohouse::class,
                ],
                [
                    'genre' => Rnb::class,
                ],
            ],
            'people'       => [
                [
                    'name'   => 'Персонаж 1490527687350',
                    'gender' => 1,
                    'skills' => [
                        [
                            'level' => 1,
                            'dance' => House::class,
                        ],
                    ],
                ],
                [
                    'name'   => 'Персонаж 1490527693502',
                    'gender' => 0,
                    'skills' => [
                        [
                            'level' => 2,
                            'dance' => Hiphop::class,
                        ],
                        [
                            'level' => 0,
                            'dance' => Electrodance::class,
                        ],
                    ],
                ],
            ],
            'current_song' => 1,
        ];
        $this->club->init($data['people'], $data['playlist'], $data['current_song']);
    }

    public function testDescribeWithFilledData()
    {
        $described = $this->club->describe();
        $this->assertArrayHasKey('genres', $described);
        $this->assertNotEmpty($described['genres']);
        $this->assertThat($described['genres'][0], $this->arrayHasKey('id'));
        $this->assertThat($described['genres'][0], $this->arrayHasKey('name'));

        $this->assertArrayHasKey('genders', $described);
        $this->assertNotEmpty($described['genders']);

        $this->assertArrayHasKey('levels', $described);
        $this->assertNotEmpty($described['levels']);

        $this->assertArrayHasKey('dances', $described);
        $this->assertNotEmpty($described['dances']);

        $this->assertArrayHasKey('playlist', $described);
        $this->assertArrayHasKey('currentSong', $described);

        $this->assertEquals(1, $described['currentSong']);

        $this->assertArrayHasKey('people', $described);
    }

}
