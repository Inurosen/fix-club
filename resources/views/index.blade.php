<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Club</title>
        <link href="{{ mix('/css/app.css') }}" rel="stylesheet">

        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>

    <nav class="navbar navbar-inverse navbar-static-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/">Club</a>
            </div>
        </div>
    </nav>

    <div class="container" role="main">
        <div id="app">
            <div class="row">
                <p>
                    <button v-if="!editMode" class="btn btn-success" @click="editMode = !editMode">Открыть клуб</button>
                    <button v-else class="btn btn-danger" @click="editMode = !editMode">Отменить</button>
                </p>
            </div>
            <newclub :levels="levels" :dances="dances" :ready="ready" :edit-mode="editMode" :genres="genres" :genders="genders"></newclub>
            <club :ready="ready" :playlist="playlist" :current-song="currentSong" :people="people"></club>
        </div>
    </div>


    <script type="application/javascript">
        window.Laravel = {
            csrfToken: '{{ csrf_token() }}'
        };
        var Club = {
            ready: false,
            editMode: false,
            genres: [],
            genders: [],
            levels: [],
            dances: [],
            playlist: [],
            currentSong: 0,
            people: []
        };
    </script>
    <script src="{{ mix('/js/app.js') }}"></script>
    </body>
</html>