
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('club', require('./components/club.vue'));
Vue.component('newclub', require('./components/newclub.vue'));

const app = new Vue({
    el: '#app',
    data: Club,
    mounted: function () {
        this.loadData();
    },
    methods: {
        loadData: function () {
            axios.get('/club')
                .then(function (response) {
                    if(response.status == 200)
                    {
                        var $data = response.data;
                        if($data.playlist != undefined && $data.playlist.constructor === Array)
                        {
                            Club.playlist = $data.playlist;
                        }
                        if($data.currentSong != undefined && !isNaN($data.currentSong))
                        {
                            Club.currentSong = $data.currentSong;
                        }
                        if($data.people != undefined && $data.people.constructor === Object)
                        {
                            Club.people = $data.people;
                        }
                        if($data.genres != undefined && $data.genres.constructor === Array)
                        {
                            Club.genres = $data.genres;
                        }
                        if($data.genders != undefined && $data.genders.constructor === Array)
                        {
                            Club.genders = $data.genders;
                        }
                        if($data.dances != undefined && $data.dances.constructor === Array)
                        {
                            Club.dances = $data.dances;
                        }
                        if($data.levels != undefined && $data.levels.constructor === Array)
                        {
                            Club.levels = $data.levels;
                        }
                    }
                    Club.ready = true;
                })
                .catch(function (error) {
                    console.log(error);
                });
        }
    }
});
