<?php

namespace App\Http\Controllers;

use App\Club\Club;
use App\Http\Requests\OpenClub;

class ClubController extends Controller {

    public function index()
    {
        return view('index');
    }

    public function describe()
    {
        $data = session('club', [
            'people'       => [],
            'playlist'     => [],
            'current_song' => 0,
        ]);
        /** @var Club $club */
        $club = Club::getInstance();
        $club->init($data['people'], $data['playlist'], $data['current_song']);
        $result = $club->describe();

        return $result;
    }

    public function openClub(OpenClub $request)
    {
        $input = $request->all();
        $config = config('club', []);
        $data = [
            'playlist'     => [],
            'people'       => [],
            'current_song' => 0,
        ];
        foreach($input['playlist'] as $playlist_item)
        {
            $data['playlist'][] = [
                'genre' => $config['genres'][$playlist_item['genre']]
            ];
        }
        foreach($input['people'] as $people_item)
        {
            $person = [
                'name' => $people_item['name'],
                'gender' => $people_item['gender'],
                'skills' => []
            ];

            foreach($people_item['skills'] as $skill_item)
            {
                $person['skills'][] = [
                    'level' => $skill_item['level'],
                    'dance' => $config['dances'][$skill_item['dance']]
                ];
            }
            $data['people'][] = $person;
        }

        session(['club' => $data]);

        return [];
    }

    public function nextSong()
    {
        $current_song = session('club.current_song', 0);
        $playlist = session('club.playlist', 0);
        if($current_song < count($playlist) - 1 AND $current_song >= 0)
        {
            $current_song++;
        }
        else
        {
            $current_song = 0;
        }

        session(['club.current_song' => $current_song]);

        return [];
    }

    public function prevSong()
    {
        $current_song = session('club.current_song', 0);
        $playlist = session('club.playlist', 0);
        if($current_song > 0)
        {
            $current_song--;
        }
        else
        {
            $current_song = count($playlist) - 1;
        }

        session(['club.current_song' => $current_song]);

        return [];
    }

}
