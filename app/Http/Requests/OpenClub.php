<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OpenClub extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'playlist'         => 'required|array',
            'playlist.*.genre' => 'required|in_genres',
            'people'           => 'required|array',

            'people.*.name'           => 'required',
            'people.*.gender'         => 'required|in_genders',
            'people.*.skills'         => 'required|array',
            'people.*.skills.*.level' => 'required|in_skill_levels',
            'people.*.skills.*.dance' => 'required|in_dances',
        ];
    }

    public function messages()
    {
        return [
            'playlist.required'          => 'Плейлист не должен быть пустым',
            'playlist.array'             => 'Плейлист имеет недопустимый формат',
            'playlist.*.genre.required'  => 'Нужно указать жанр песни',
            'playlist.*.genre.in_dances' => 'Жанр песни указан неверно',

            'people.required'                     => 'Нужно добавить хотя бы одного персонажа',
            'people.array'                        => 'Список персонажей имеет недопустимый формат',
            'people.*.name.required'              => 'Нужно указать имя персонажа',
            'people.*.gender.required'            => 'Нужно указать пол персонажа',
            'people.*.gender.in_genders'          => 'Пол персонажа указан неверно',
            'people.*.skills.required'            => 'Нужно указать навыки персонажа',
            'people.*.skills.array'               => 'Список навыков имеет недопустимый формат',
            'people.*.skills.*.level.required'    => 'Нужно указать предпочтение персонажа',
            'people.*.skills.*.level.skill_level' => 'Предпочтение указано неверно',
            'people.*.skills.*.dance.required'    => 'Нужно указать танец',
            'people.*.skills.*.dance.in_dances'   => 'Танец указан неверно',
        ];
    }
}
