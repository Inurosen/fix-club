<?php

namespace App\Providers;

use App\Club\Person;
use App\Club\Skill;
use Illuminate\Support\ServiceProvider;
use Validator;

class AppServiceProvider extends ServiceProvider {
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('in_genres', function($attribute, $value, $parameters, $validator)
        {
            $genres = config('club.genres', []);

            return isset($genres[$value]);
        });
        Validator::extend('in_genders', function($attribute, $value, $parameters, $validator)
        {
            return in_array($value, [ Person::GENDER_FEMALE, Person::GENDER_MALE ]);
        });
        Validator::extend('in_skill_levels', function($attribute, $value, $parameters, $validator)
        {
            return in_array($value, [ Skill::LEVEL_AMATEUR, Skill::LEVEL_SKILLED, Skill::LEVEL_PRO ]);
        });
        Validator::extend('in_dances', function($attribute, $value, $parameters, $validator)
        {
            $dances = config('club.dances', []);

            return isset($dances[$value]);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
