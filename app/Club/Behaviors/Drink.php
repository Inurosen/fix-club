<?php

namespace App\Club\Behaviors;

use App\Club\Behavior;
use App\Singleton;

/**
 * Class Drink
 * Поведение - Возлияния
 * @package App\Club\Behaviors
 */
abstract class Drink implements Behavior {

    use Singleton;

    public function describeMovements()
    {
        return 'пьет';
    }
}