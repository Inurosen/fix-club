<?php

namespace App\Club\Behaviors\Drinks;

use App\Club\Behaviors\Drink;
use App\Singleton;

/**
 * Class Vodka
 * Водка
 * @package App\Club\Behaviors\Drinks
 */
class Vodka extends Drink {
    use Singleton;

    public function describeMovements()
    {
        return 'пьет водку';
    }
}