<?php

namespace App\Club\Behaviors\Dances;

use App\Club\Behaviors\Dance;
use App\Club\BodyParts\Body;
use App\Club\BodyParts\Foot;
use App\Club\BodyParts\Hand;
use App\Club\BodyParts\Head;
use App\Club\Movement;
use App\Singleton;

/**
 * Class Pop
 * Танец Pop
 * @package App\Club\Behaviors\Dances
 */
class Pop extends Dance {

    use Singleton;

    protected $_name = 'Поп';

    private function __construct()
    {
        $this->_body_parts_movements = [
            Head::class => Movement::SMOOTH_SWAYING,
            Body::class => Movement::SMOOTH_SWAYING,
            Hand::class => Movement::SMOOTH_SWAYING,
            Foot::class => Movement::SMOOTH_SWAYING,
        ];
    }

}