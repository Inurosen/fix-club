<?php

namespace App\Club\Behaviors\Dances;

use App\Club\Behaviors\Dance;
use App\Club\BodyParts\Body;
use App\Club\BodyParts\Foot;
use App\Club\BodyParts\Hand;
use App\Club\BodyParts\Head;
use App\Club\Movement;
use App\Singleton;

/**
 * Class House
 * Танец House
 * @package App\Club\Behaviors\Dances
 */
class House extends Dance {

    use Singleton;

    protected $_name = 'Хаус';

    private function __construct()
    {
        $this->_body_parts_movements = [
            Head::class => Movement::SLIGHT_MOVEMENT,
            Body::class => Movement::SIDE_SHAKING,
            Hand::class => Movement::SMOOTH_SWAYING,
            Foot::class => Movement::RHYTHMICAL,
        ];
    }

}