<?php

namespace App\Club\Behaviors\Dances;

use App\Club\Behaviors\Dance;
use App\Club\BodyParts\Body;
use App\Club\BodyParts\Foot;
use App\Club\BodyParts\Hand;
use App\Club\BodyParts\Head;
use App\Club\Movement;
use App\Singleton;

/**
 * Class Rnb
 * Танец Rnb
 * @package App\Club\Behaviors\Dances
 */
class Rnb extends Dance {

    use Singleton;

    protected $_name = 'Rnb';

    private function __construct()
    {
        $this->_body_parts_movements = [
            Head::class => Movement::FRONT_SHAKING,
            Body::class => Movement::RHYTHMICAL,
            Hand::class => Movement::SPINNING,
            Foot::class => Movement::SLIGHT_MOVEMENT,
        ];
    }

}