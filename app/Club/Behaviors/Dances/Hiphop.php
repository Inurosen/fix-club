<?php

namespace App\Club\Behaviors\Dances;

use App\Club\Behaviors\Dance;
use App\Club\BodyParts\Body;
use App\Club\BodyParts\Foot;
use App\Club\BodyParts\Hand;
use App\Club\BodyParts\Head;
use App\Club\Movement;
use App\Singleton;

/**
 * Class Hiphop
 * Танец Hiphop
 * @package app\Club\Behaviors\Dances
 */
class Hiphop extends Dance {

    use Singleton;

    protected $_name = 'Хип-хоп';

    private function __construct()
    {
        $this->_body_parts_movements = [
            Head::class => Movement::FRONT_SHAKING,
            Body::class => Movement::FRONT_SHAKING,
            Hand::class => Movement::BENT,
            Foot::class => Movement::HALF_SQUATTING,
        ];
    }

}