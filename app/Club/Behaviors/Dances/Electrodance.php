<?php

namespace App\Club\Behaviors\Dances;

use App\Club\Behaviors\Dance;
use App\Club\BodyParts\Body;
use App\Club\BodyParts\Foot;
use App\Club\BodyParts\Hand;
use App\Club\BodyParts\Head;
use App\Club\Movement;
use App\Singleton;

/**
 * Class Electrodance
 * Танец Electrodance
 * @package App\Club\Behaviors\Dances
 */
class Electrodance extends Dance {

    use Singleton;

    protected $_name = 'Электродэнс';

    private function __construct()
    {
        $this->_body_parts_movements = [
            Head::class => Movement::SLIGHT_MOVEMENT,
            Body::class => Movement::FRONT_SHAKING,
            Hand::class => Movement::SPINNING,
            Foot::class => Movement::RHYTHMICAL,
        ];
    }

}