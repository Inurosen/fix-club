<?php

namespace App\Club\Behaviors;

use App\Club\Behavior;
use App\Singleton;

/**
 * Class Dance
 * Поведение - Танец
 * @package App\Club\Behaviors
 */
abstract class Dance implements Behavior {

    use Singleton;

    /**
     * @var array Части тела и движения ими, используемые в танце.
     */
    protected $_body_parts_movements = [];

    /**
     * @var static Название танца
     */
    protected $_name = 'Танец';

    public function describeMovements()
    {
        return 'танцует ' . $this->_name;
    }

    /**
     * @return static
     */
    public function getName()
    {
        return $this->_name;
    }
}