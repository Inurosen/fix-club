<?php

namespace App\Club;

/**
 * Interface Behavior
 * Интерфейс поведения персонажей в разных зонах
 * @package App\Club
 */
interface Behavior {
    public function describeMovements();
}