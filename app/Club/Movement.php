<?php

namespace App\Club;

/**
 * Class Movement
 * Движение в танце
 * @package app\Club
 */
class Movement {
    const FRONT_SHAKING = 'shaking back and forward';
    const SIDE_SHAKING = 'shaking right and left';
    const SMOOTH_SWAYING = 'swaying smoothly';
    const HALF_SQUATTING = 'half-squatting';
    const RHYTHMICAL = 'moving rhythmically';
    const BENT = 'bent';
    const SPINNING = 'spinning';
    const SLIGHT_MOVEMENT = 'moving slightly';
    const NO_MOVEMENT = 'not moving';
}