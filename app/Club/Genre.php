<?php

namespace App\Club;

use App\Singleton;

/**
 * Class Genre
 * Жанр песни
 * @package App\Club
 */
abstract class Genre {

    use Singleton;

    /**
     * @var string Имя жанра
     */
    protected $_name = 'Жанр';

    /**
     * @var array Танец
     */
    protected $_dances = [];

    /**
     * @return array
     */
    public function getDances()
    {
        return $this->_dances;
    }

    public function getName()
    {
        return $this->_name;
    }
}