<?php

namespace App\Club;

use App\Club\Behaviors\Drinks\Vodka;
use App\Singleton;

/**
 * Class Club
 * @package app\Club
 */
class Club {

    use Singleton;

    /**
     * @var array Люди, находящиеся в клубе
     */
    private $_people = [];
    /**
     * @var array Плейлист клуба
     */
    private $_playlist = [];
    /**
     * @var int Индекс текущей песни из плейлиста
     */
    private $_current_song = 0;
    /**
     * @var array Зоны в клубе
     */
    private $_areas = [];

    public function init($people = [], $playlist = [], $current_song = 0)
    {
        $this->_current_song = $current_song;
        $areas = config('club.areas', []);

        foreach($areas as $areas_item)
        {
            $area = new $areas_item();
            $this->_areas[] = $area;
        }

        foreach($playlist as $playlist_item)
        {
            $genre = $playlist_item['genre']::getInstance();
            $this->_playlist[] = new Song($genre);
        }
        foreach($people as $people_item)
        {
            $skills = [];
            foreach($people_item['skills'] as $skill_item)
            {
                $dance = $skill_item['dance']::getInstance();
                $skills[] = new Skill($skill_item['level'], $dance);
            }
            $person = new Person($people_item['name'], $people_item['gender'], $skills, Vodka::getInstance()); // Предположим, что все они по-умолчанию любят водку
            $this->_people[] = $person;
        }
        $this->populateAreas();
    }

    private function populateAreas()
    {
        /** @var Person $person */
        foreach($this->_people as $person)
        {
            /** @var Area $area */
            foreach($this->_areas as $area)
            {
                if($area->validPerson($person))
                {
                    $person->setArea($area);
                    $area->addPerson($person);
                    break 1;
                }
            }
        }
    }

    /**
     * @return Song
     */
    public function getCurrentSongFromPlaylist()
    {
        return $this->_playlist[$this->_current_song];
    }

    /**
     * Описать что происходит в клубе
     * @return array
     */
    public function describe()
    {
        $genres_config = config('club.genres', []);
        $genres = [];
        foreach($genres_config as $key => $value)
        {
            $genres[] = [
                'id' => $key,
                'name' => $value::getInstance()->getName(),
            ];
        }

        $dances_config = config('club.dances', []);
        $dances = [];
        foreach($dances_config as $key => $value)
        {
            $dances[] = [
                'id' => $key,
                'name' => $value::getInstance()->getName(),
            ];
        }

        $result = [
            'genres' => $genres,
            'genders' => [
                Person::GENDER_FEMALE => 'Ж',
                Person::GENDER_MALE => 'М',
            ],
            'levels' => [
                Skill::LEVEL_AMATEUR => 'Любит танцевать',
                Skill::LEVEL_SKILLED => 'Может танцевать',
                Skill::LEVEL_PRO => 'Умеет танцевать',
            ],
            'dances' => $dances,
            'playlist' => [],
            'currentSong' => $this->_current_song,
            'people' => [
                'total' => 0,
                'persons' => [],
            ],
        ];

        /** @var Song $song */
        foreach($this->_playlist as $song)
        {
            $result['playlist'][] = [
                'genre' => $song->getGenre()->getName(),
            ];
        }

        if(count($this->_people) === 0)
        {
            return $result;
        }

        $result['people']['total'] = count($this->_people);

        /** @var Person $people_item */
        foreach($this->_people as $people_item)
        {
            $person = [
                'name' => $people_item->getName(),
                'activity' => '',
                'gender' => $people_item->getGenderDescription(),
            ];
            $person['activity'] = $people_item->getActivity()->describeMovements();

            $result['people']['persons'][] = $person;
        }

        return $result;
    }
}