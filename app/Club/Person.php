<?php

namespace App\Club;

use App\Club\Behaviors\Dance;
use App\Club\Behaviors\Drink;

/**
 * Class Person
 * Перонаж
 * @package App\Club
 */
class Person {

    const GENDER_FEMALE = 0;
    const GENDER_MALE = 1;

    /**
     * @var string Имя персонажа
     */
    private $_name;
    /**
     * @var int Пол персонажа
     */
    private $_gender;
    /**
     * @var array Навыки персонажа
     */
    private $_skills = [];

    /**
     * @var Drink Что пьет
     */
    private $_drinks;

    /**
     * @var Area Где сейчас находится
     */
    private $_area;

    /**
     * @var Behavior Поведение персонажа в данный момент
     */
    private $_activity;

    function __construct($name, $gender, $skills, $drinks)
    {
        $this->_name = $name;
        $this->_gender = $gender;
        $this->_skills = $skills;
        $this->_drinks = $drinks;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->_name;
    }

    /**
     * @return int
     */
    public function getGender()
    {
        return $this->_gender;
    }

    /**
     * @return string
     */
    public function getGenderDescription()
    {
        return $this->_gender == static::GENDER_MALE ? 'М' : 'Ж';
    }

    /**
     * @return array
     */
    public function getSkills()
    {
        return $this->_skills;
    }

    /**
     * Имеется ли у персонажа указанный навык?
     * @param $required_skill
     * @return Behavior
     * @throws \Exception
     */
    public function getSkill($required_skill)
    {
        /** @var Skill $skill */
        foreach($this->_skills as $skill)
        {
            /** @var Club $club */
            $club = Club::getInstance();
            if($skill->getDance() instanceof $required_skill
                AND in_array($skill->getDance(), $club->getCurrentSongFromPlaylist()->getGenre()->getDances())
            )
            {
                return $skill->getDance();
            }
        }

        throw new \Exception('Person doesn\'t have required skill.');
    }

    /**
     * @return Area
     */
    public function getArea()
    {
        return $this->_area;
    }

    /**
     * @param Area $area
     */
    public function setArea($area)
    {
        $this->_area = $area;
    }

    /**
     * @return Behavior
     */
    public function getActivity()
    {
        return $this->_activity;
    }

    /**
     * @param Behavior $activity
     */
    public function setActivity(Behavior $activity)
    {
        $this->_activity = $activity;
    }

    /**
     * @return Drink
     */
    public function getDrinks()
    {
        return $this->_drinks;
    }

}