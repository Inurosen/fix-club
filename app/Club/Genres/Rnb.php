<?php

namespace App\Club\Genres;

use App\Club\Behaviors\Dances\Hiphop as DanceHiphop;
use App\Club\Behaviors\Dances\Rnb as DanceRnb;
use App\Club\Genre;
use App\Singleton;

/**
 * Class Rnb
 * Жанр Rnb
 * @package App\Club\Genres
 */
class Rnb extends Genre {

    use Singleton;

    protected $_name = 'Rnb';

    private function __construct()
    {
        $this->_dances = [
            DanceRnb::getInstance(),
            DanceHiphop::getInstance(),
        ];
    }
}