<?php

namespace App\Club\Genres;

use App\Club\Behaviors\Dances\Electrodance as DanceElectrodance;
use App\Club\Behaviors\Dances\House as DanceHouse;
use App\Club\Genre;
use App\Singleton;

/**
 * Class Electrohouse
 * Жанр Electrohouse
 * @package App\Club\Genres
 */
class Electrohouse extends Genre {

    use Singleton;

    protected $_name = 'Электрохаус';

    private function __construct()
    {
        $this->_dances = [
            DanceElectrodance::getInstance(),
            DanceHouse::getInstance(),
        ];
    }
}