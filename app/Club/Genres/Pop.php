<?php

namespace App\Club\Genres;

use App\Club\Behaviors\Dances\Pop as DancePop;
use App\Club\Genre;
use App\Singleton;

/**
 * Class Pop
 * Жанр Pop
 * @package App\Club\Genres
 */
class Pop extends Genre {

    use Singleton;

    protected $_name = 'Поп';

    private function __construct()
    {
        $this->_dances = [
            DancePop::getInstance(),
        ];
    }
}