<?php

namespace App\Club;
use App\Club\Behaviors\Dance;

/**
 * Class Skill
 * Навыки танца персонажа
 * @package App\Club
 */
class Skill {

    /**
     * Любитель. Любит танцевать, но не очень умеет.
     */
    const LEVEL_AMATEUR = 0;
    /**
     * Умелец. Может неплохо танцевать.
     */
    const LEVEL_SKILLED = 1;
    /**
     * Профессионал. Умеет хорошо танцевать.
     */
    const LEVEL_PRO = 2;

    /**
     * @var int Уровень навыка.
     */
    private $_level;

    /**
     * @var Dance Танец
     */
    private $_dance;

    function __construct($level, Dance $dance)
    {
        $this->_level = $level;
        $this->_dance = $dance;
    }

    /**
     * @return Dance
     */
    public function getDance()
    {
        return $this->_dance;
    }

    /**
     * @return int
     */
    public function getLevel()
    {
        return $this->_level;
    }
}