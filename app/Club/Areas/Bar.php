<?php

namespace App\Club\Areas;

use App\Club\Area;
use App\Club\Behaviors\Drink;
use App\Club\Person;

/**
 * Class Bar
 * Зона бара в клубе
 * @package App\Club\Areas
 */
class Bar extends Area {

    function __construct()
    {
        $this->_behavior = Drink::class;
    }

    public function validPerson(Person $person)
    {
        $person->setActivity($person->getDrinks());

        return true;
    }
}