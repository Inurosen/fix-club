<?php

namespace App\Club\Areas;

use App\Club\Area;
use App\Club\Behaviors\Dance;
use App\Club\Person;

/**
 * Class DanceFloor
 * Зона танцпола в клубе
 * @package App\Club\Areas
 */
class DanceFloor extends Area {

    function __construct()
    {
        $this->_behavior = Dance::class;
    }

    public function validPerson(Person $person)
    {
        try
        {
            $dance = $person->getSkill($this->_behavior);
            $person->setActivity($dance);

            return true;
        }
        catch(\Exception $e)
        {
            return false;
        }
    }

}