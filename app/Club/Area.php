<?php

namespace App\Club;

abstract class Area {

    /**
     * @var array Люди в данной зоне
     */
    protected $_people = [];

    /**
     * @var Behavior Что люди делают в этой зоне
     */
    protected $_behavior;

    public function addPerson(Person $person)
    {
        $this->_people[] = $person;
    }

    /**
     * @return array
     */
    public function getPeople()
    {
        return $this->_people;
    }

    /**
     * @return Behavior
     */
    public function getBehavior()
    {
        return $this->_behavior;
    }

    public function validPerson(Person $person)
    {
        return false;
    }
}