<?php

namespace App\Club;

/**
 * Class Song
 * Песня
 * @package App\Club
 */
class Song {
    /**
     * @var int Продолжительность в секундах
     */
    private $_duration = 0;

    /**
     * @var Genre Жанр
     */
    private $_genre;

    function __construct(Genre $genre)
    {
        $this->_genre = $genre;
    }

    /**
     * @return Genre Жанр
     */
    public function getGenre()
    {
        return $this->_genre;
    }
}