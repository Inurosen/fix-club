<?php

namespace App;

trait Singleton {
    private static $uniqueInstance = null;

    private function __construct()
    {
    }

    private function __clone()
    {
    }
    public function __wakeup()
    {
        throw new \Exception('Cannot unserialize singleton');
    }
    public static function getInstance()
    {
        if(static::$uniqueInstance === null)
        {
            static::$uniqueInstance = new static();
        }

        return static::$uniqueInstance;
    }
}
