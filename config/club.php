<?php
/**
 * Конфиги для клуба
 */

return [
    'body_parts' => [
        App\Club\Person::class => [
            App\Club\BodyParts\Body::class,
            App\Club\BodyParts\Head::class,
            App\Club\BodyParts\Hand::class,
            App\Club\BodyParts\Foot::class,
        ],
        // Тут еще могут быть животные. Например:
        'Animal'               => [
            App\Club\BodyParts\Body::class,
            App\Club\BodyParts\Head::class,
            App\Club\BodyParts\Hand::class,
            App\Club\BodyParts\Foot::class,
            App\Club\BodyParts\Tail::class,
        ],
    ],
    'movements'  => [
        App\Club\BodyParts\Body::class => [
            App\Club\Movement::FRONT_SHAKING,
            App\Club\Movement::SMOOTH_SWAYING,
        ],
        App\Club\BodyParts\Head::class => [
            App\Club\Movement::FRONT_SHAKING,
            App\Club\Movement::SLIGHT_MOVEMENT,
            App\Club\Movement::SMOOTH_SWAYING,
        ],
        App\Club\BodyParts\Hand::class => [
            App\Club\Movement::BENT,
            App\Club\Movement::SPINNING,
            App\Club\Movement::SMOOTH_SWAYING,
        ],
        App\Club\BodyParts\Foot::class => [
            App\Club\Movement::HALF_SQUATTING,
            App\Club\Movement::RHYTHMICAL,
            App\Club\Movement::SMOOTH_SWAYING,
        ],
        App\Club\BodyParts\Tail::class => [
        ],
    ],
    'areas' => [
        App\Club\Areas\DanceFloor::class,
        App\Club\Areas\Bar::class,
    ],
    'genres' => [
        sha1(App\Club\Genres\Electrohouse::class) => App\Club\Genres\Electrohouse::class,
        sha1(App\Club\Genres\Rnb::class) => App\Club\Genres\Rnb::class,
        sha1(App\Club\Genres\Pop::class) => App\Club\Genres\Pop::class,
    ],
    'dances' => [
        sha1(App\Club\Behaviors\Dances\Electrodance::class) => App\Club\Behaviors\Dances\Electrodance::class,
        sha1(App\Club\Behaviors\Dances\House::class) => App\Club\Behaviors\Dances\House::class,
        sha1(App\Club\Behaviors\Dances\Hiphop::class) => App\Club\Behaviors\Dances\Hiphop::class,
        sha1(App\Club\Behaviors\Dances\Pop::class) => App\Club\Behaviors\Dances\Pop::class,
        sha1(App\Club\Behaviors\Dances\Rnb::class) => App\Club\Behaviors\Dances\Rnb::class,
    ]
];